﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{
    class Rectangle : Shape
    {
        int width, length;
        public Rectangle() : base()
        {
            //width = 100;
            //height = 100;
        }
        public override void set(Color penColor, bool fill, params int[] list)
        {
            base.set(penColor, fill, list[0], list[1]);
            this.length = list[2];
            this.width = list[3];
        }

        public override void draw(Graphics g)
        {
            Pen pen = new Pen(penColor, 1);


            if (fill == true)
            {
                SolidBrush brush = new SolidBrush(penColor);

                g.FillRectangle(brush, posX, posY, length, width);
            }
            g.DrawRectangle(pen, posX, posY, length, width);
        }
    }
}
