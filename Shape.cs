﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{  public abstract class Shape:Shapes
    {
        Canvas c;
        protected Color penColor; //shape's colour
        protected int posX, posY; //not I could use c# properties for this
        protected bool fill;

        public Shape()
        {

        }


        public void setX(int posX)
        {

            this.posX = posX;
        }


        public int getX()
        {
            return this.posX;
        }


        public Shape(Color penColor, bool fill, int posX, int posY)
        {

            this.penColor = penColor;
            this.fill = fill;
            this.posX = posX;
            this.posY = posY;
        }


        public abstract void draw(Graphics g);

        public virtual void set(Color penColor, bool fill, params int[] list)
        {
            this.penColor = penColor;
            this.fill = fill;
            this.posX = list[0];
            this.posY = list[1];
        }
    }
}
