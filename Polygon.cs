﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{
    class Polygon : Shape
    {
        int[] parameters;
        private Color penColor;

        public Polygon()
        {
        }


        public override void set(Color penColor, bool fill, params int[] list)
        {
            base.set(penColor, fill, list[0], list[1]);
            parameters = list;
        }


        public override void draw(Graphics g)
        {
            Point[] polygonPoints = new Point[parameters.Length / 2];
            int index = 0;


            for (int i = 0; i < parameters.Length; i += 2)
            {
                polygonPoints[index] = new Point(parameters[i], parameters[i + 1]);
                index++;
            }

            Pen pen = new Pen(penColor, 1);

            if (fill == true)
            {
                SolidBrush brush = new SolidBrush(penColor);
                g.FillPolygon(brush, polygonPoints);
            }

            g.DrawPolygon(pen, polygonPoints);
        }
    }
}






