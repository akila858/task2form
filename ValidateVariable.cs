﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{
    public static class ValidateVariable
    {
        public static String[] Var = new String[4];
       public enum VarType
        {
            integer,
            stringData,

        }
        public static bool ValidateVar(string STR)
        {
            //int c=mohamed
            Var = STR.Split(' ');
            bool IsExist = VarIsExist(Var[1]);
            switch (Var[0])
            {
                case "int":
                    if (ValidateString(Var[3]) == VarType.integer)
                    {
                        if (!IsExist)
                            Parser.VB.Add(Var[1], Convert.ToInt32(Var[3]));
                        else Parser.VB[Var[1]] = Convert.ToInt32(Var[3]);
                    }
                    else
                    {
                        Parser.Exceptions += (Environment.NewLine + "Cannot add string in ineger var");
                    }
                    return true;
                case "string":
                    if (!IsExist)
                        Parser.VB.Add(Var[1],Var[3].ToString());
                    else Parser.VB[Var[1]] = Var[3].ToString();
                    return true;
                default:
                    return false;
            }
        }
        public static VarType ValidateString(string STR)
        {
            bool IsInteger = true;
            foreach (var item in STR)
            {
                if (!Char.IsDigit(item)) IsInteger = false;
            }
            return (IsInteger?VarType.integer:VarType.stringData);
        }
        public static bool VarIsExist(string VAR)
        {
            foreach (var item in Parser.VB.Keys)
            {
                if(item == VAR) return true;
            }
            return false;
        }
        public static bool IsNumber(this string str)
        {
            foreach (var item in str)
            {
                if (!Char.IsDigit(item)) return false;
                    
            }
            return true;
        }
       

        public static int GetVarIsExist(string size2)
        {
            int value = 0;
            if (!size2.IsNumber())
            {
                foreach (var s in Parser.VB.Keys)
                {
                    if (s == size2)
                    {
                        value = Convert.ToInt32(Parser.VB[s]);
                        return value;
                    }
                }
            }
            else value = Convert.ToInt32(size2);
            return value;
        }

    }
}
