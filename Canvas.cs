﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Task2Form
{
    public class Canvas

    {

        const int XSIZE = 640;
        const int YSIZE = 480;
        public bool fill;
        public Graphics g;
        public static IDictionary<String, int> storeVariables = new Dictionary<String, int>(); //to store variables 
        protected Color background_colour = Color.Gray; //background color for the canvas
        public bool variableFlag = false;
        int parameter1;
        int parameter2;
        int radius;
        public Pen pen = new Pen(Color.Black, 1);
        public int posX;
        public int posY;
        public static ArrayList errorList = new ArrayList();
        Shape s;
        ShapeFactory factory = new ShapeFactory();
        PerformIF ifCommand = new PerformIF();
        PerformLoop loopCommand = new PerformLoop();
        bool ifConditionFlag = false;
        public bool conditionNotMatched = false;
        bool thenFlag = false;
        bool loopConditionFlag = false;
        public bool loopConditionMatched = false;
        String whileCommand;
        bool whileFlag = false;
        bool ifFlag = false;
        int[] parametersList = new int[8];
        bool methodFlag = false;
        PerformMethod MethodCommand = new PerformMethod();
        Parser parser = new Parser();
        String methodName;
        public Color penColor;

        /// <summary>
        ///constructor with intial values for the canvas, pen and point and no fill required
        /// </summary>
            public Canvas()
            {
                TextBox text = new TextBox();
                this.g = text.CreateGraphics();// to create new canvas(grapgics and can be done by create instance graphics g =new graphics()

            //initial values
                posX = 0;
                posY = 0;
                penColor = Color.Black;
                fill = false;

            }

        /// <summary>
        /// the constructor for the bitmap with the initial values
        /// </summary>
        /// <param name="g"></param>
            public Canvas(Graphics g)
            {
                //initial values
                this.g = g;
                posX = 0;
                posY = 0;
                penColor = Color.Black;
                fill = false;
            }

        /// <summary>
        /// clear method to clear the canvas
        /// </summary>
            public void Clear()
            {
                g.Clear(background_colour);
        }

        /// <summary>
        /// Moveto input will be assigned here
        /// </summary>
        /// <param name="toX">the pointx will move to this point</param>
        /// <param name="toY">the pointy will move to this point</param>
        public void MoveTo(int toX, int toY)
            {
                posX = toX;
                posY = toY;
            }


        /// <summary>
        /// to reset the point to the point (0,0)
        /// </summary>
            public void ResetCanvasPoint()
            {
                posX = 0;
                posY = 0;
            }

        /// <summary>
        /// Drawto method
        /// </summary>
        /// <param name="new_clr">new colour will change in case pen color changes</param>
        /// <param name="toX"> x point will move to this poit</param>
        /// <param name="toY"> Y point will move to this poit</param>
        public void DrawTo(Color new_clr, int toX, int toY)
            {
                pen.Color = new_clr;
                g.DrawLine(pen, posX, posY, toX, toY);
                posX = toX;
                posY = toY;
            }

        /// <summary>
        /// METHOD to draw rectangel
        /// </summary>
        /// <param name="new_clr">new colour will change in case pen color changes</param>
        /// <param name="fill">if fill true the shape will be filled</param>
        /// <param name="length">rectangle length</param>
        /// <param name="width">hiegh for rectangle</param>
        public void DrawRectangle(Color new_clr, bool fill, int length, int width)
        {
            s = factory.getShape("rectangle");
            s.set(new_clr, fill, posX, posY, length, width); // method to set the values
            s.draw(g);
        }

        /// <summary>
        /// draw polygon method
        /// </summary>
        /// <param name="new_clr">new colour will change in case pen color changes</param>
        /// <param name="fill">if fill true the shape will be filled</param>
        /// <param name="parameters">the parameters to draw polygon</param>
        public void DrawPolygon(Color new_clr, bool fill, String[] parameters)
        {
            s = factory.getShape("polygon");
            int[] parametersList = new int[parameters.Length]; // can accept the number of lines to draw the polygon
            int index = 0;
            foreach (String element in parameters)
            {
                parametersList[index] = Convert.ToInt32(element);
                index++;
            }

            s.set(new_clr, fill, parametersList);
            s.draw(g);
        }

        /// <summary>
        /// draw circle method
        /// </summary>
        /// <param name="new_clr">new colour will change in case pen color changes</param>
        /// <param name="fill">if fill true the shape will be filled</param>
        /// <param name="radius">the value of circle radius</param>
        public void DrawCircle(Color new_clr, bool fill, int radius)
        {
            s = factory.getShape("circle");
            s.set(new_clr, fill, posX, posY, radius);
            s.draw(g);
        }

        /// <summary>
        /// draw trainagle method
        /// </summary>
        /// <param name="new_clr">new colour will change in case pen color changes</param>
        /// <param name="fill">f fill true the shape will be filled</param>
        public void DrawTriangle(Color new_clr, bool fill)
        {
            s = factory.getShape("triangle");
            s.set(new_clr, fill, posX, posY, posX + 80, posY + 80);
            s.draw(g);
        }

        /// <summary>
        /// clear the error list 
        /// </summary>
        public void clearErrorList()
        {
            errorList.Clear();
        }

        /// <summary>
        /// clear the dictionary
        /// </summary>
        public void clearDictionary()
        {
            storeVariables.Clear();
        }

        /// <summary>
        /// to split the excepression 
        /// </summary>
        /// <param name="varExpression">will take the variable expression</param>
        /// <returns></returns>
        public String[] parseVariableExpression(String varExpression)
        {
            String[] parsedVarExp = varExpression.Split(' ');//how the variables will be split

            return parsedVarExp;
        }

        /// <summary>
        /// spllit the expression by =
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public String[] splitExpression(String expression)
        {
            String[] splittedExpression = expression.Split('='); //how expression will be seprated

            return splittedExpression;
        }



        /// <summary>
        /// splet the papameter with ","
        /// </summary>
        /// <param name="parameters">the parameter wich must be splitted</param>
        /// <returns></returns>
        public String[] ParameterParser(String parameters)
        {
            String[] parseParameter = parameters.Split(',');// how parameter will be seprated

            return parseParameter;
        }

        /// <summary>
        /// the process method to process the commands 
        /// </summary>
        /// <param name="programCommand">the command inside the program window </param>
        /// <param name="lineCounter">the program lines counter</param>
        /// <param name="syntaxCheck">this flag to check if there is error</param>
        public void programProcess(String programCommand, int lineCounter, bool syntaxCheck)
        {
            String tempName = programCommand.Split('(')[0];
            String declareVarName = programCommand.Split(' ')[0]; //variable name to be declare and split with " "

            if (declareVarName.Equals("var"))// if the first declare is var then it is variable 
            {
                variableFlag = true;        //vairal flag true 
                programCommand = programCommand + " 1";          //the program will sratr next line 

                try
                {
                    String[] ParseVarExpression = parseVariableExpression(programCommand);//parse the vaiarble expression for each line
                    String expression = ParseVarExpression[1]; //second element will be the expression
                    String[] parseExpression = splitExpression(expression);// array will parse the expression
                    String varName = parseExpression[0];// first element to store the name
                    String varValue = parseExpression[1];// second element to store the valuse
                    int intVarValue = Convert.ToInt32(varValue);// convert it to integer

                    if (!syntaxCheck)
                    {
                        storeVariables.Add(varName, intVarValue);
                    }
                }
                catch (FormatException)
                {
                    errorList.Add("Error at line " + lineCounter + ". Please Enter in correct syntax (EX: var<space><varName>=<varValue>)'");
                }
                catch (IndexOutOfRangeException)
                {
                    errorList.Add("Error at line " + lineCounter + ". Please Enter in correct syntax (EX: var<space><varName>=<varValue>)'");
                }
            }

            
            if (declareVarName.Equals("if")) // incase if keyword then if statment 

            {
                ifFlag = true;

                ifConditionFlag = ifCommand.checkIfCommand(programCommand, lineCounter);// if and replace the values in the method

                if (programCommand.Contains("then")) //if the condition contatin then the flag is true
                {
                    thenFlag = true;
                }

                if (ifConditionFlag) //ther is matching condition if it is true 
                {
                    conditionNotMatched = false; // the condition match
                }
                else
                {
                    conditionNotMatched = true;// the condition dosent match
                }
            }
            else if (ifFlag)
            {
                if (conditionNotMatched) // if the condtion dosenot match
                {
                    if (programCommand.Equals("endif"))// if there is endif then the condition match
                    {
                        conditionNotMatched = false;
                        ifFlag = false;
                    }

                    if (thenFlag)
                    {
                        conditionNotMatched = false;// the then flag will be same as condition match
                        ifFlag = false;
                    }
                }
                else
                {
                    shapeCommands(programCommand, lineCounter, syntaxCheck);

                    // Checks if the current line has keyword 'endif' and performs the task underneath
                    if (programCommand.Equals("endif"))
                    {
                        conditionNotMatched = false;
                        ifFlag = false;
                    }
                }
            }
            else if (thenFlag && !conditionNotMatched)// if then flag true and the condition match will execute old command 
            {
                shapeCommands(programCommand, lineCounter, syntaxCheck); 
                thenFlag = false;
            }
            else if (declareVarName.Equals("while")) // if the command while 
            {
                loopConditionFlag = loopCommand.checkLoopCommand(programCommand, lineCounter);// the loop will be checket

                if (loopConditionFlag)
                {
                    whileCommand = programCommand; //while command is thenFlag program command and will be execute 
                    loopConditionMatched = true;
                }
                else
                {
                    loopConditionMatched = false;
                }

                whileFlag = true;
            }

            else if (whileFlag)
            {
                ArrayList temp_List = new ArrayList(); // array to assign the values tempararly
                //values to be assined
                temp_List.Add(g);
                temp_List.Add(posX);
                temp_List.Add(posY);
                temp_List.Add(pen);

                whileFlag = loopCommand.executeLoop(whileFlag, loopConditionMatched, whileCommand, programCommand, lineCounter, syntaxCheck, temp_List);
            }
            else if (declareVarName.Equals("method"))// if the command is method
            {
                methodFlag = MethodCommand.identifyMethod(programCommand, syntaxCheck, lineCounter);//idetify the method in this line
                methodName = MethodCommand.methodName; // method name will be assigned here
            }
            else if (methodFlag)
            {
                methodFlag = MethodCommand.storeMethodCommands(programCommand);
            }
            else if (programCommand.Equals(methodName + "()"))// method without parameter
            {
                foreach (String eachLine in MethodCommand.methodCommands)
                {
                    programProcess(eachLine, lineCounter, syntaxCheck);//execute commands as normal
                }
            }
            else if (tempName.Equals(methodName))//if the method name was called  
            {
                try
                {
                    String[] callMethod = programCommand.Split('(');
                    String method_Parameters = programCommand.Split('(', ')')[1];// the paramete what inside ()
                    String[] parameters = method_Parameters.Split(','); //parameter1 will be splitted with,

                    if (callMethod[0].Equals(methodName))// calling the method
                    {
                        if (parameters.Length == MethodCommand.parsedParameters.Length)
                        {
                            for (int index = 0; index < parameters.Length; index++)
                            {
                                //convert the parameter for the method to integer
                                storeVariables[MethodCommand.parsedParameters[index]] = Convert.ToInt32(parameters[index]); 
                                if (storeVariables.ContainsKey(MethodCommand.parsedParameters[index]))
                                {
                                    MethodCommand.parsedParameters[index] = storeVariables[MethodCommand.parsedParameters[index]].ToString();
                                }
                            }
                            foreach (String eachLine in MethodCommand.methodCommands)//each line will be process and execute 
                            {
                                programProcess(eachLine, lineCounter, syntaxCheck);
                            }
                        }
                        else
                        {
                            throw new InvalidCommand("Invalid Method Command");
                        }
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    errorList.Add("Error at line:  " + lineCounter + ". Please Enter Correct Method statement");
                }
                catch (InvalidCommand)
                {
                    errorList.Add("Error at line: " + lineCounter + ". Please Enter Correct Method statement");
                }

            }


            else if (programCommand.Contains("+")) // caculate the expressions + add the elments together
            {
                String[] expParameter = programCommand.Split('+');

                if (storeVariables.ContainsKey(expParameter[0])) // Checks if the first parameter in the string array is a variable stored in data dictionary and performs
                {
                    if (storeVariables.ContainsKey(expParameter[1]))// if the second element contains variables from Dectionary 
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] + storeVariables[expParameter[1]]; // add the 2 values 
                    }
                    else
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] + Convert.ToInt32(expParameter[1]);//else to calculate the stored variables with the converted second part
                    }
                }
                else
                {
                    if (storeVariables.ContainsKey(expParameter[1]))
                    {
                        storeVariables[expParameter[1]] = storeVariables[expParameter[1]] + Convert.ToInt32(expParameter[0]);// two values to be add
                    }
                }
            }
            else if (programCommand.Contains("-"))
            {
                String[] expParameter = programCommand.Split('-');// caculate the expressions + add the elments together

                if (storeVariables.ContainsKey(expParameter[0]))// if the stored variables match the dictionary
                {
                    if (storeVariables.ContainsKey(expParameter[1]))// if the second element match the stored vaiable 
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] - storeVariables[expParameter[1]];//take awy the first element from the secpnd element
                    }
                    else
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] - Convert.ToInt32(expParameter[1]);//first elment will het the value from taking away firest element from second element
                    }
                }
                else
                {
                    if (storeVariables.ContainsKey(expParameter[1]))
                    {
                        storeVariables[expParameter[1]] = storeVariables[expParameter[1]] - Convert.ToInt32(expParameter[0]);//first element = firste element sutract from second element 
                    }
                }
            }

            else if (programCommand.Contains("*"))
            {
                String[] expParameter = programCommand.Split('*');

                if (storeVariables.ContainsKey(expParameter[0]))// if the variable stored in the dictionary match
                {
                    if (storeVariables.ContainsKey(expParameter[1]))//if the second element from stored variables match
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] * storeVariables[expParameter[1]];//the first element will first element by second element
                    }
                    else
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] * Convert.ToInt32(expParameter[1]);// the second element will be the result from first element byt second element
                    }
                }
                else
                {
                    if (storeVariables.ContainsKey(expParameter[1]))
                    {
                        storeVariables[expParameter[1]] = storeVariables[expParameter[1]] * Convert.ToInt32(expParameter[0]);
                    }
                }
            }
            else if (programCommand.Contains("/"))
            {
                String[] expParameter = programCommand.Split('/');

                if (storeVariables.ContainsKey(expParameter[0]))
                {
                    if (storeVariables.ContainsKey(expParameter[1]))
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] / storeVariables[expParameter[1]];
                    }
                    else
                    {
                        storeVariables[expParameter[0]] = storeVariables[expParameter[0]] / Convert.ToInt32(expParameter[1]);
                    }
                }
                else
                {
                    if (storeVariables.ContainsKey(expParameter[1]))
                    {
                        storeVariables[expParameter[1]] = storeVariables[expParameter[1]] / Convert.ToInt32(expParameter[0]);
                    }
                }
            }
            else
            {
                shapeCommands(programCommand, lineCounter, syntaxCheck);// command will be checked if it is shape
            }
        }

            public void shapeCommands(String programCommand, int lineCounter, bool syntaxCheck)
            {
                String[] splittedCommand = parser.CommandSplitter(programCommand);

                String command = splittedCommand[0];
            String parameters;



                if (programCommand.Equals("triangle") || programCommand.Equals("endif") || programCommand.Contains("+") || programCommand.Contains("-") || programCommand.Contains("*") || programCommand.Contains("/"))
                {
                    programCommand = programCommand + " 1";
                }

                try
                {
                    

                    if (command.Equals("pen"))
                    {
                    parameters = splittedCommand[1];

                    if (parameters.Equals("red"))
                        {

                            if (!syntaxCheck)
                            {
                                penColor = Color.Red;
                            }
                        }

                        else if (parameters.Equals("blue"))
                        {

                            if (!syntaxCheck)
                            {
                                penColor = Color.Blue;
                            }
                        }

                        else if (parameters.Equals("yellow"))
                        {

                            if (!syntaxCheck)
                            {
                                penColor = Color.Yellow;
                            }
                        }

                        else if (parameters.Equals("green"))
                        {

                            if (!syntaxCheck)
                            {
                                penColor = Color.Green;
                            }
                        }

                        else
                        {
                            penColor = Color.Black;

                            errorList.Add("Error at line:  " + lineCounter + ". Please Enter a valid color - 'red', 'blue', 'yellow' or 'green'");
                        }
                    }

                    else if (command.Equals("fill"))// for fill the shape 
                    {

                        parameters = splittedCommand[1];
                    if (parameters.Equals("on"))// when the command fill on __fill the shape 
                        {
                            if (!syntaxCheck)
                            {
                                fill = true;
                            }
                        }
                        else if (parameters.Equals("off"))// when the command fill off __ remove the fill the shape
                        {

                            if (!syntaxCheck)
                            {
                                fill = false;
                            }
                        }
                        else
                        {
                            fill = false;

                            errorList.Add("Errorr at Line: " + lineCounter + ". Please Enter valid parameter - 'on' or 'off'");
                        }
                    }

                    else if (command.Equals("circle"))
                    {
                        try
                        {
                        parameters = splittedCommand[1];
                        if (storeVariables.ContainsKey(parameters))
                            {
                                radius = Convert.ToInt32(storeVariables[parameters]);
                            }
                            else
                            {
                                radius = Convert.ToInt32(parameters);
                            }


                            if (!syntaxCheck)
                            {
                                DrawCircle(penColor, fill, radius);
                            }
                        }
                        catch (FormatException)
                        {
                            errorList.Add("Error at line: " + lineCounter + ". Please Enter Numeric Value for Radius.");
                        }
                        catch (IndexOutOfRangeException)
                        {
                            errorList.Add("Error at line: " + lineCounter + ". Radius is missing.");
                        }

                    }


                    else if (command.Equals("tri"))
                    {

                        if (!syntaxCheck)
                        {
                            DrawTriangle(penColor, fill);
                        }
                    }

                    else if (command.Equals("polygon"))
                    {
                    parameters = splittedCommand[1];
                    String[] splittedParameters = ParameterParser(parameters);
                    try
                    {
                        if (!syntaxCheck)
                        {
                            DrawPolygon(penColor, fill, splittedParameters);
                        }
                    }
                    catch (ArgumentException)
                    {
                        errorList.Add("Error at line:   " + lineCounter + ". Parameter is not valid.");
                    }
                    }


                    else if (command.Equals("moveto"))//
                    {
                        try
                        {
                        parameters = splittedCommand[1];
                        String[] splittedParameters = ParameterParser(parameters);


                            if (Regex.IsMatch(splittedParameters[0], @"[a-z]"))
                            {

                                if (storeVariables.ContainsKey(splittedParameters[0]))
                                {

                                    parameter1 = Convert.ToInt32(storeVariables[splittedParameters[0]]);
                                }
                            }
                            else
                            {
                                parameter1 = Convert.ToInt32(splittedParameters[0]);
                            }

                            if (Regex.IsMatch(splittedParameters[1], @"[a-z]"))
                            {
                                if (storeVariables.ContainsKey(splittedParameters[1]))
                                {
                                    parameter2 = Convert.ToInt32(storeVariables[splittedParameters[1]]);
                                }
                            }
                            else
                            {
                                parameter2 = Convert.ToInt32(splittedParameters[1]);
                            }

                            if (!syntaxCheck)
                            {
                                MoveTo(parameter1, parameter2);
                            }
                        }
                        catch (FormatException)
                        {
                            errorList.Add("Error at line:  " + lineCounter + ". Please Enter Numeric Value for coordinates.");
                        }
                        catch (IndexOutOfRangeException)
                        {
                            errorList.Add("Error at line:   " + lineCounter + ". Please Enter Two Numeric Values for coordinatessplit with (,).");
                        }
                    }

                    else if (command.Equals("drawto"))
                    {
                        try
                        {
                        parameters = splittedCommand[1];
                        String[] splittedParameters = ParameterParser(parameters);

                            if (Regex.IsMatch(splittedParameters[0], @"[a-z]"))
                            {
                                if (storeVariables.ContainsKey(splittedParameters[0]))
                                {
                                    parameter1 = Convert.ToInt32(storeVariables[splittedParameters[0]]);
                                }
                            }
                            else
                            {
                                parameter1 = Convert.ToInt32(splittedParameters[0]);
                            }

                            if (Regex.IsMatch(splittedParameters[1], @"[a-z]"))
                            {
                                if (storeVariables.ContainsKey(splittedParameters[1]))
                                {
                                    parameter2 = Convert.ToInt32(storeVariables[splittedParameters[1]]);
                                }
                            }
                            else
                            {
                                parameter2 = Convert.ToInt32(splittedParameters[1]);
                            }

                            if (!syntaxCheck)
                            {

                                DrawTo(penColor, parameter1, parameter2);
                            }
                        }
                        catch (FormatException)
                        {
                            errorList.Add("Error at line:   " + lineCounter + ". Please Enter Numeric Value for coordinates.");
                        }
                        catch (IndexOutOfRangeException)
                        {
                            errorList.Add("Error at line:  " + lineCounter + ". Please Enter Two Numeric Values for coordinates.");
                        }
                    }

                    else if (command.Equals("rect"))
                    {
                        try
                        {
                        parameters = splittedCommand[1];
                        String[] splittedParameters = ParameterParser(parameters);


                            if (Regex.IsMatch(splittedParameters[0], @"[a-z]"))
                            {

                                if (storeVariables.ContainsKey(splittedParameters[0]))
                                {
                                    parameter1 = Convert.ToInt32(storeVariables[splittedParameters[0]]);
                                }
                            }
                            else
                            {
                                parameter1 = Convert.ToInt32(splittedParameters[0]);
                            }


                            if (Regex.IsMatch(splittedParameters[1], @"[a-z]"))
                            {
                                if (storeVariables.ContainsKey(splittedParameters[1]))
                                {
                                    parameter2 = Convert.ToInt32(storeVariables[splittedParameters[1]]);
                                }
                            }
                            else
                            {
                                parameter2 = Convert.ToInt32(splittedParameters[1]);
                            }


                            if (!syntaxCheck)
                            {
                                DrawRectangle(penColor, fill, parameter1, parameter2);
                            }
                        }
                        catch (FormatException)
                        {
                            errorList.Add("Error at line:   " + lineCounter + ". Please Enter Numeric Value for coordinates.");
                        }
                        catch (IndexOutOfRangeException)
                        {
                            errorList.Add("Error at line:   " + lineCounter + ". Please Enter Two Numeric Values for coordinates split with (,).");
                        }
                    }
                    else if (command.Equals("var") || command.Equals("endif") || command.Contains("+") || command.Contains("-") || command.Contains("*") || command.Contains("/"))
                    {

                    }
                    else
                    {
                        errorList.Add("Error at line: " + lineCounter + ". Please Enter Valid Command.");
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    errorList.Add("Error at line: " + lineCounter + ". Please Enter Valid Command.");// error list is adding wrong command 
                }
            }
        }
    }


