﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{

    public class DrawTo : Shape
    {
        protected int xpos,ypos, x,y;
        Canvas canvas;
        public Color new_clr;

           
        public DrawTo() : base()
        {
           
        }
        /// <summary>
        /// move drawing cursor to x, y
        /// </summary>
        /// <param name="c"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>

        public DrawTo(Canvas c, int x, int y) : base()
        {
            //  Name = "MoveTo";
            this.xpos = x;
            this.ypos = y;

        }
        /// <summary>
        /// setting the canvas and cusor to any given details
        /// </summary>
        /// <param name="c"></param>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Set(Color colour, bool fill, int x, int y)
        {
            this.new_clr = colour;
            this.xpos = x;
            this.ypos = y;
        }
        /// <summary>
        /// execute the command
        /// </summary>
        /// <returns></returns>
        //public override bool Execute()
        //{
        //    c.MoveTo(xpos, ypos);
        //}

        //public override void ParseParameters(int[] ParameterList)
        //{
        //    throw new NotImplementedException();
        //}

        //public override string ToString()
        //{
        //    return base.ToString() + "DrawTo " + xpos + "," + ypos;
        //}

        public override void draw(Graphics g)
        {
           Pen Pen = new Pen(new_clr, 1);

            g.DrawLine(Pen, x, y, xpos, ypos);
        
        }

      
    }
}

