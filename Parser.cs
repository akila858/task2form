﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task2Form.Method;

namespace Task2Form
{
    public class Parser
    {
        String expression = "";// for variables to be computing
        DataTable dt = new DataTable(); 
        public static String[] sut = new String[2];
        public static Canvas MyCanvas;
        public static List<MethodClass> methods = new List<MethodClass>();
        public static String[] line;
        public static string Exceptions = "";
        public static ShapeFactory factory = new ShapeFactory();
        public static Dictionary<string, object> VB = new Dictionary<string, object>();
        public static Shape s;
        public static  Pen Pen = new Pen(Color.Black, 1);
        public static ArrayList shapes = new ArrayList();
        public static int poX, posY = 0;
        public static bool fill;
        public static String programd;
        public static Color colour;
        //Point p = new Point(poX,posY);
        //int programCounter = 0;
        //int loopCounter = 0;
        //int loopSize = 0;
        //bool loopFlag = false;
        //int variableCounter = 0;
        //int methodCounter = 0;
        //bool methodFlag = false;
        //bool methodExecuting = false;
        //int saveProgramCounter = 0;
        //bool executeLinesFlag = true; //used for ifs
        


        public Parser()
        { 
        
        }
/// <summary>
/// constructor  to allow used canvas
/// </summary>
/// <param name="c">the canvas to be used as graphics</param>
        public Parser (Canvas c)
        {
            MyCanvas = c;
        }

      public static void Clear(Canvas c)
        {
            MyCanvas = c;
            
        }
        public String[] CommandSplitter(String programCommand)
        {
            String[] splittedCommand = programCommand.Split(' ');

            return splittedCommand;
        }

        public  void ParseCommand(string command)
        {

            ArrayList var1 = new ArrayList();
            String[] split = new String[2];
            int x = 0;
            int y = 0;

            try
            {
                if (command.Equals("line"))
                {

                    String[] param = sut[1].Split(',');

                    int[] it = Array.ConvertAll(param, int.Parse);
                    // MyCanvas.DrawTo(colour,fill, it[0], it[1]);
                    x = it[0];
                    y = it[1];
                }
                else if (command.Equals("reset"))
                { }
                else if (command.Equals("circle"))
                {
                    try
                    {
                        Pen.Color = Color.Red;


                        var ser = factory.getShape(nameof(Circle));
                        int radius = Convert.ToInt32((String)sut[1]);
                        //int xc = x- size ;
                        //int yc = y- size ;
                        ser.set(Color.Black, fill, poX, posY, radius);
                        shapes.Add(ser);
                    }
                    catch (FormatException)
                    {
                        // ". Please Enter Numeric Value for Radius.");
                    }
                    catch (IndexOutOfRangeException)
                    {
                        //errorList.Add("Error at line: " + lineCounter + ". Radius is missing.");
                    }
                }


                else if (command.Equals("drawto"))
                {
                    s = factory.getShape("drawto");
                    String[] param1 = sut[1].Split(',');
                    int[] it1 = Array.ConvertAll(param1, int.Parse);
                    s.set(colour, fill, x, y, it1[0], it1[1]);
                    shapes.Add(s);// 

                }

                else if (command.Equals("moveto"))
                {
                    //s = factory.getShape("moveto");
                    var ls = factory.getShape(nameof(MoveTo));
                    String[] param4 = sut[1].Split(',');
                    int[] it4 = Array.ConvertAll(param4, int.Parse);
                    Logic.StartPoint.X = it4[0];
                    Logic.StartPoint.Y = it4[1];
                    ls.set(Color.Green, fill, it4[0], it4[1]);
                    shapes.Add(ls);// 
                    poX = it4[0];
                    posY = it4[1];
                }

                else if (command.Equals("tri"))
                {// Draw Triangle 
                    s = factory.getShape(nameof(Triangle));
                    String[] param2 = sut[1].Split(',');
                    int[] it2 = Array.ConvertAll(param2, int.Parse);
                    s.set(colour, fill, x, y, it2[0], it2[1]);
                    shapes.Add(s);// 
                    poX = x;
                    posY = y;
                }

                else if (command.Equals("rect"))
                {
                    s = factory.getShape(nameof(Rectangle));
                    String[] param3 = sut[1].Split(',');
                    int[] it3 = Array.ConvertAll(param3, int.Parse);
                    s.set(colour, fill, x, y, it3[0], it3[1]);
                    shapes.Add(s);//
                    poX = x;
                    posY = y;
                }

                else if (command.Equals("clear"))
                {
                    MyCanvas = new Canvas();
                    shapes = new ArrayList();
                    //Parser.Clear(new Canvas());
                }

                else if (command.Equals("reset"))
                {
                    poX = posY = 0;
                }

                else if (command.Equals("pen") && sut[1].Equals("red"))
                {

                    Pen.Color = Color.Red;

                }



                else if (command.Equals("Save"))
                {
                    SaveFileDialog save = new SaveFileDialog();
                    if (save.ShowDialog() == DialogResult.OK)
                    {
                        File.WriteAllText(save.FileName, MyCanvas.ToString());
                    }
                }

                else if (command.Equals("load"))
                {
                    // MyCanvas.Clear();
                }

                else if (command.Equals("run"))
                {

                    for (int i = 0; i < line.Length; i++)
                    {

                        split = line[i].Split(' ');
                        programd = split[0];
                        // var line = programd.Split('\n');
                        //x = poX;
                        //y = posY;

                        if (programd.Equals("fill"))
                        {




                        }
                        else if (programd.Equals("moveto"))
                        {
                            var ls = factory.getShape(nameof(MoveTo));
                            String[] param = split[1].Split(',');
                            //string[] it4 = Array.ConvertAll(param4, string);
                            int value1 = ValidateVariable.GetVarIsExist(param[0]);
                            int value2 = ValidateVariable.GetVarIsExist(param[1]);
                            ls.set(colour, fill, value1, value2);
                            shapes.Add(ls);
                            poX = value1;
                            posY = value2;
                        }
                        else if (programd.Equals("rect"))
                        {
                            x = poX;
                            y = posY;
                            s = factory.getShape(nameof(Rectangle));
                            String[] param = split[1].Split(',');
                            //   int[] it5 = Array.ConvertAll(param5, int.Parse);
                            int value1 = ValidateVariable.GetVarIsExist(param[0]);
                            int value2 = ValidateVariable.GetVarIsExist(param[1]);
                            s.set(colour, fill, x, y, value1, value2);
                            shapes.Add(s);
                            poX = x;
                            posY = y;

                        }
                        else if (programd.Equals("circle"))
                        {
                            s = factory.getShape(nameof(Circle));
                            int value = ValidateVariable.GetVarIsExist(split[1].Trim());
                            s.set(colour, fill, x, y, value);
                            shapes.Add(s);

                            //poX = x;
                            //posY = y;
                        }
                        else if (programd.Equals("fill") && split[2].Equals("on"))
                        {

                        }
                        else if (programd.StartsWith("method"))
                        {
                            //define begin of method keyword and methodend
                            int Startindex = 0;
                            int Endindex = 0;

                            String Documentation = "";// To make what insie method execute
                            // creat object to creat a method
                            CreateMethod method = new CreateMethod();
                            for (int s = 0; s < line.Length; s++)
                            {
                                if (line[s].StartsWith("method".ToLower())) Startindex = s;
                                else if (line[s].StartsWith("endmethod".ToLower())) Endindex = s;
                            }
                            for (int q = Startindex; q <= Endindex; q++)
                            {
                                Documentation += line[q] + Environment.NewLine;
                            }
                            method.Create(Documentation);// documetaion will be treat as method body
                        }
                        //else if (line.StartsWith("int"))
                        //{

                        //}
                        //else if (programd.StartsWith("string"))
                        //{
                        //    ValidateVariable.ValidateString(programd);
                        //}
                        else if (programd.Equals("if") || programd.Equals("while"))
                        {

                        }
                        else if (programd.Equals(var1.ToString()) && split[1].Equals("="))

                        {
                            DataTable dt = new DataTable();


                        }

                        foreach (var item in line)
                        {
                            if (item.StartsWith("int") || item.StartsWith("string"))
                                ValidateVariable.ValidateVar(item);
                        }



                    }
                }
            }
            catch (ApplicationException e)
            {

                throw new ApplicationException("\nunkown command ");
            }




            /// <summary>
            /// 
            /// </summary>
            String ParseProgram(String program, bool execute)
            {
                bool errors = false;
                String errorList = "";
                String[] lines = program.Split('\n');


                for (int i = 0; i < lines.Length; i++)
                {
                    try
                    {
                        ParseCommand(lines[i]);
                    }

                    catch (ApplicationException e)
                    {
                        errorList += " " + e.Message + "at line " + (i + 1);
                        //executing was not implemented 
                        execute = false;

                    }
                }
                return errorList;
            }
        }
        
            




         
        


    }

}