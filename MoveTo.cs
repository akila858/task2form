﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{
    
    public class MoveTo : Shape
    {
        protected int xpos, ypos,x,y;
        Color new_clr;
        public bool fill;
        public MoveTo()
        {

        }
        /// <summary>
        /// move drawing cursor to x, y
        /// </summary>
        /// <param name="c"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>

        public MoveTo(Color color, bool fill, int x, int y) : base()
        {
           this.new_clr = color;
            this.fill = fill;
            this.xpos = x;
            this.ypos = y;

        }
        /// <summary>
        /// setting the canvas and cusor to any given details
        /// </summary>
        /// <param name="c"></param>
        /// <param name="name"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Set(Canvas c, bool fill,int x, int y)
        {
           // base.Set(c, fill);
            this.xpos = x;
            this.ypos = y;
        }
        /// <summary>
        /// execute the command
        /// </summary>
        /// <returns></returns>
        //public override bool Execute()
        //{
        //    c.MoveTo(xpos, ypos);
        //}

        //public override void ParseParameters(int[] ParameterList)
        //{
        //    throw new NotImplementedException();
        //}

        //public override string ToString()
        //{
        //    return base.ToString() + "MoveTo " + xpos + "," + ypos;
        //}

        public override void draw(Graphics g)
        {
            Pen Pen = new Pen(Color.Crimson, 2);
            g.DrawLine(Pen, xpos, ypos, x, y);
           
        }

       
    }
}

