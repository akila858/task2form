﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form.Method
{
    public abstract class BaseMethod : IBaseMethod
    {
        public abstract void Create(string g);

        public abstract void Run(string MethodName);
    }
}
