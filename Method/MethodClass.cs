﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form.Method
{
    public class MethodClass
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Documentations { get; set; }
        public object Parameter { get; set; }
    }
}
