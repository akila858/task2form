﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form.Method
{
    public class CreateMethod : BaseMethod
    {
        public  MethodClass method = new MethodClass();
        public  String[] Var = new String[4];
        public CreateMethod()
        {

        }
        public override void Create(string STR)
        {
            //int c=mohamed
            Var = STR.Replace("\r","").Split('\n');
            string MethodName = Var[0].Split('(')[0].Split(' ')[1];
            method.Name = MethodName;
            method.ID = (Parser.methods.Max(x=>(int?) x.ID)??0)+1;
            string Parameter = Var[0].Split('(')[1].Replace(")","");
            for (int i = 1; i < Var.Length -2; i++)
            {
                method.Documentations += Var[i]+"\n";
            }
            Parser.methods.Add(method); 
        }

        public override void Run(string MethodName)
        {
            
        }


        //public static bool ValidateString(string STR)
        //{
        //    bool IsInteger = true;
        //    foreach (var item in STR)
        //    {
        //        if (!Char.IsDigit(item)) IsInteger = false;
        //    }
        //    return IsInteger ;
        //}
        //public static bool VarIsExist(string VAR)
        //{
        //    foreach (var item in Parser.VB.Keys)
        //    {
        //        if (item == VAR) return true;
        //    }
        //    return false;
        //}
        //public static bool IsNumber(this string str)
        //{
        //    foreach (var item in str)
        //    {
        //        if (!Char.IsDigit(item)) return false;

        //    }
        //    return true;
        //}


        //public static int GetVarIsExist(string size2)
        //{
        //    int value = 0;
        //    if (!size2.IsNumber())
        //    {
        //        foreach (var s in Parser.VB.Keys)
        //        {
        //            if (s == size2)
        //            {
        //                value = Convert.ToInt32(Parser.VB[s]);
        //                return value;
        //            }
        //        }
        //    }
        //    else value = Convert.ToInt32(size2);
        //    return value;
        //}
    }
}
