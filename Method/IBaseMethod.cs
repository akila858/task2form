﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form.Method
{
    public interface IBaseMethod
    {
        void Create(string STR);

        void Run(string MethodName);
    }
}
