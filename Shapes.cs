﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{
    public interface Shapes
    {
        void set(Color c, bool fill, params int[] list);
        void draw(Graphics g);

    }
}
