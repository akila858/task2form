﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace Task2Form
{
    public partial class Form1 : Form
    {

        /// <summary>
        /// Main Properties
        /// </summary>

        #region MainProperty

        Bitmap OutBitmap = new Bitmap(640, 480);  // bitmap object to be created when the program run with specific size 
        Canvas myCanvas;                         //canvas object for drawing
        Parser Parser = new Parser();           //object created from Parser class
        bool SyntaxCheck = false;              //flag to check if there is syntax error will be true

        

        /// <summary>
        ///main Constructor when the program loading will run these commands
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            myCanvas = new Canvas(Graphics.FromImage(OutBitmap));  // Canvas for the project were assinged to the bitmap
            myCanvas.Clear();                                     // load with clear canvas

        }
        #endregion Constractor

        #region Property


        #endregion

        #region Helper


        // paint method are choosen to draw shapes
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            //settings for drawing 
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutBitmap, 0, 0);

            for (int i = 0; i < Parser.shapes.Count; i++)
            {
                // Shape s;
                Parser.s = (Shape)Parser.shapes[i];
                if (Parser.s != null)
                {
                    //the shape will be drawn 
                    Parser.s.draw(g);
                    Console.WriteLine(Parser.s.ToString());//shape name will be written in the console
                }
                else
                    Console.WriteLine("invalid shape in array"); //ALL SHAPES MUST BE DECLARE CORRECTLY




            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        
        /// <summary>
        /// the method that take the programs and define the act that must be done
        /// </summary>
        /// <param name="programCommand">it the string that will be assign to this method</param>
        public void programProcess(String programCommand)
        {
            if (programCommand.Equals("clear"))// if the command clear the will clear all the canvas
            {
                myCanvas.Clear();
            }
            else if (programCommand.Equals("reset"))// reset command to retun the point for the pen to pint(0,0) 
            {
                myCanvas.ResetCanvasPoint();
            }

            else if (programCommand.Equals("run"))// to run the commands that wrtten in the program window
            {
                String[] programd = programWindow.Lines; // the program will be read by lines from the input window
                int PC = 1;


                foreach (String line in programd) // for each line in the program check the line and count the line
                {
                    int counter = PC++;
                    myCanvas.programProcess(line, counter, SyntaxCheck);// canvas method to count the lines and check validation
                }
            }

            else
            {
                myCanvas.programProcess(programCommand, 1, SyntaxCheck);
            }
        }

        #endregion
/// <summary>
/// Syntax Button to check the commands without runing the program
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
        private void syntaxButton_Click(object sender, EventArgs e)
        {
            SyntaxCheck = true;
            String errorText = errorWindow.Text = "";     //error will be cheched from output window(richtextbox1)
            myCanvas.Clear(); 
            myCanvas.clearDictionary();                  // clear the dictionary in case of variables or method 

            String[] ProgramLines = programWindow.Lines;// the program will be read by lines from program window
            int PC = 1;
            foreach (String line in ProgramLines)
            {
                int Counter = PC++;
                myCanvas.programProcess(line, Counter, SyntaxCheck);// check the program anf count the lines 
            }

            myCanvas.ResetCanvasPoint();                           // reset the canvas point
            errorWindow.ForeColor  = Color.Green;
            myCanvas.fill = false;

            foreach (String error in Canvas.errorList)    // errorlist is static so no need to make instance(object)
            {
                errorWindow.AppendText(error + "\n"); // EACH LINE has an error will be identified i
            }

            if (Canvas.errorList.Count == 0)         // when the is no error then the next message will appear
            {
                errorWindow.AppendText("No syntax error, the program can be run");// the outpt if the conmmands are corrct will display this message
            }
            SyntaxCheck = false;
        }


        /// <summary>
        /// Exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();// when click exit the program will be closed
        }
    

        /// <summary>
        /// load the program and despay in program window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadProgramMenu_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Choose File";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    String lines;
                    String filePath = openFileDialog.FileName;
                    StreamReader streamFile = File.OpenText(filePath);// withh give th file path
                    while ((lines = streamFile.ReadLine()) != null)
                    {
                        programWindow.Text += lines + "\n"; //counting Line
                    }
                }
                catch (IOException)
                {
                    MessageBox.Show("IO Error occured");
                }
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
       

        /// <summary>
        /// save the program text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveProgramMenu_Click_1(object sender, EventArgs e)
        {
            // to save the program 
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Save Program Text";
            saveFileDialog.ShowDialog();


            if (saveFileDialog.FileName != "")
            {// savre the text in file so user can use it again
                String filePath = saveFileDialog.FileName;
                StreamWriter writeString = File.CreateText(filePath);
                String[] saveProgramText = programWindow.Lines;

                foreach (string line in saveProgramText)
                {// save program line by line
                    writeString.WriteLine(line);
                }
                writeString.Close();
            }
        }

        /// <summary>
        ///commandLine to execute all commands 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String command = commandLine.Text;
                command = command.Trim().ToLower();
                
                errorWindow.Text = ""; //error will be display in the error Area
                errorWindow.ForeColor = Color.Red; // the colour of the font 
                myCanvas.clearErrorList();
                myCanvas.clearDictionary();

                
                programProcess(command);

                foreach (String eachError in Canvas.errorList) // each error will be writing the line and what is the error
                {
                    errorWindow.AppendText(eachError + "\n");// the writing will be disaply in the error Area with red font
                }
                commandLine.Text = "";
                Refresh();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void commandLine_TextChanged(object sender, EventArgs e)
        {

        }

        private void runButton_Click(object sender, EventArgs e)
        {


            errorWindow.Text = ""; //error will be display in the error Area
            errorWindow.ForeColor = Color.Red; // the colour of the font 
            myCanvas.clearErrorList();
            myCanvas.clearDictionary();


            programProcess("run");// using only the command run

            foreach (String eachError in Canvas.errorList) // each error will be writing the line and what is the error
            {
                errorWindow.AppendText(eachError + "\n");// the writing will be disaply in the error Area with red font
            }
            commandLine.Text = "";
            Refresh();

        }
    }
}
