﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2Form
{
   public  class Circle : Shape
    {
        int radius;

        public Circle() : base()
        {

        }
        public override void set(Color penColor, bool fill, params int[] list)
        {
            base.set(penColor, fill, list[0], list[1]);

            this.radius = list[2];
        }


        public override void draw(Graphics g)
        {

            Pen pen = new Pen(penColor, 1);


            if (fill == true)
            {
                SolidBrush brush = new SolidBrush(penColor);

                g.FillEllipse(brush, posX - radius, posY - radius, radius * 2, radius * 2);
            }

            g.DrawEllipse(pen, posX - radius, posY - radius, radius * 2, radius * 2);
        }
    }
}


