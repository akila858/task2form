﻿namespace Task2Form
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.runButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Resultlabel = new System.Windows.Forms.Label();
            this.programWindow = new System.Windows.Forms.RichTextBox();
            this.syntaxButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProgramMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProgramMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.commandLine = new System.Windows.Forms.TextBox();
            this.errorWindow = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(126, 390);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(85, 36);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(438, 69);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(475, 369);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // Resultlabel
            // 
            this.Resultlabel.AutoSize = true;
            this.Resultlabel.Location = new System.Drawing.Point(62, 218);
            this.Resultlabel.Name = "Resultlabel";
            this.Resultlabel.Size = new System.Drawing.Size(0, 20);
            this.Resultlabel.TabIndex = 6;
            // 
            // programWindow
            // 
            this.programWindow.Location = new System.Drawing.Point(68, 69);
            this.programWindow.Name = "programWindow";
            this.programWindow.Size = new System.Drawing.Size(335, 184);
            this.programWindow.TabIndex = 8;
            this.programWindow.Text = "";
            // 
            // syntaxButton
            // 
            this.syntaxButton.Location = new System.Drawing.Point(245, 390);
            this.syntaxButton.Name = "syntaxButton";
            this.syntaxButton.Size = new System.Drawing.Size(82, 36);
            this.syntaxButton.TabIndex = 9;
            this.syntaxButton.Text = "Syntax";
            this.syntaxButton.UseVisualStyleBackColor = true;
            this.syntaxButton.Click += new System.EventHandler(this.syntaxButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 33);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadProgramMenu,
            this.saveProgramMenu,
            this.exitMenu});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(54, 29);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // loadProgramMenu
            // 
            this.loadProgramMenu.Name = "loadProgramMenu";
            this.loadProgramMenu.Size = new System.Drawing.Size(258, 34);
            this.loadProgramMenu.Text = "Load Program File";
            this.loadProgramMenu.Click += new System.EventHandler(this.loadProgramMenu_Click_1);
            // 
            // saveProgramMenu
            // 
            this.saveProgramMenu.Name = "saveProgramMenu";
            this.saveProgramMenu.Size = new System.Drawing.Size(258, 34);
            this.saveProgramMenu.Text = "Save Program";
            this.saveProgramMenu.Click += new System.EventHandler(this.saveProgramMenu_Click_1);
            // 
            // exitMenu
            // 
            this.exitMenu.Name = "exitMenu";
            this.exitMenu.Size = new System.Drawing.Size(258, 34);
            this.exitMenu.Text = "Exit";
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(66, 259);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(337, 26);
            this.commandLine.TabIndex = 11;
            this.commandLine.TextChanged += new System.EventHandler(this.commandLine_TextChanged);
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // errorWindow
            // 
            this.errorWindow.Location = new System.Drawing.Point(66, 301);
            this.errorWindow.Name = "errorWindow";
            this.errorWindow.Size = new System.Drawing.Size(337, 83);
            this.errorWindow.TabIndex = 12;
            this.errorWindow.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 531);
            this.Controls.Add(this.errorWindow);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.syntaxButton);
            this.Controls.Add(this.programWindow);
            this.Controls.Add(this.Resultlabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.runButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Resultlabel;
        private System.Windows.Forms.RichTextBox programWindow;
        private System.Windows.Forms.Button syntaxButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadProgramMenu;
        private System.Windows.Forms.ToolStripMenuItem saveProgramMenu;
        private System.Windows.Forms.ToolStripMenuItem exitMenu;
        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.RichTextBox errorWindow;
    }
}

