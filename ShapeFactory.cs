﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2Form.Method;

namespace Task2Form
{
    public class ShapeFactory
    {
        /// <summary>
        /// The factory clas has only one method to create instance for the classes
        /// </summary>
        /// <param name="shapeType"></param>
        /// <returns></returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim(); 

            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();
            }       
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else if (shapeType.Equals("POLYGON"))
            {
                return new Polygon();
            }
            else if (shapeType.Equals("DRAWTO"))
            {
                return new DrawTo();
            }
            else if (shapeType.Equals("MOVETO"))
            {
                return new MoveTo();
            }
            else
            {
                //if we get here then what has been passed in is inkown so throw an appropriate exception
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }


            public BaseMethod getShape(string shapeType,string DocumentationMethod)
        {
            shapeType = shapeType.ToUpper().Trim(); 

            switch (shapeType)
            {
                case nameof(CreateMethod):
                    return new CreateMethod();
                case nameof(RunMethod):
                    return new RunMethod();
                default:
                    System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                    throw argEx;
            }
        }
    }
}
